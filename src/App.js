
import './App.css';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './Components/Menu';
import Card1 from './Components/Card1';


function App() {
  return (
    <div>
      <Menu />
      <Card1 />
    
    </div>

  );
}

export default App;
