
import React, { Component } from 'react'
import { Card, Button } from 'react-bootstrap'

export default class Card1 extends Component {
    constructor() {
        super()

        this.state={
            isShow:true
        }
       function OnDisplay(){
            this.setState({
                isShow:false
            })

        }


        this.state = {
            car: [
                {
                    image: "https://www.cstatic-images.com/car-pictures/xl/USC50LGC051A021001.png",
                    title: "Lamborghini",
                    desc: "Lamborghini new 2021"
                },
                {
                    image: "https://s.aolcdn.com/commerce/autodata/images/USC90BMC681A021001.jpg",
                    title: "BMW",
                    desc: "BMW new 2021",
                },
                {
                    image: "https://www.driving.co.uk/s3/st-driving-prod/uploads/2018/08/Rolls-Royce-Phantom-Privacy-Suite-01.jpg",
                    title: "Rolls royce",
                    desc: "Rolls royce new 2021",
                }
            ]
        }
    }

//style={{display:this.state.isShow?"block":"none"}}

    render() {

        return (
            <div class="container-fluid w-75 mt-5 ">
                <div className="row"  >
                    {this.state.car.map((item, index) => {
                        return (
                            <div className="col-lg-4"  style={{display:this.state.isShow?"none":"block"}} >
                                <Card className="mr-2" style={{ width: '18rem' }}>
                                    <Card.Img variant="top" src={item.image} />
                                    <Card.Body>
                                        <Card.Title>{item.title}</Card.Title>
                                        <Card.Text>
                                            {item.desc}
                                        </Card.Text>
                                        <Button variant="primary" onClick={this.OnDisplay}>Delect</Button>
                                    </Card.Body>
                                </Card>
                            </div>
                        )
                    })}
                </div>
            </div>

        );
    }
}





